package pl.kaczanowski.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kaczanowski.car.model.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {

}
