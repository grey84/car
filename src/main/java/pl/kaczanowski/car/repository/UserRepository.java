package pl.kaczanowski.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kaczanowski.car.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    boolean findByLogin(String login);
}
