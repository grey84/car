package pl.kaczanowski.car.controller.mapper;

import org.springframework.stereotype.Component;
import pl.kaczanowski.car.controller.dto.CreateUserDto;
import pl.kaczanowski.car.controller.dto.UserDto;
import pl.kaczanowski.car.model.User;

@Component
public class UserDtoMapper {

    public UserDto mappingToDto(User user) {

        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .login(user.getLogin())
                .email(user.getEmail())
                .build();
    }

    public User mappingToModel(CreateUserDto createUserDto) {

        return User.builder()
                .login(createUserDto.getLogin())
                .password(createUserDto.getPassword())
                .firstName(createUserDto.getFirstName())
                .lastName(createUserDto.getLastName())
                .email(createUserDto.getEmail())
                .build();
    }
}
