package pl.kaczanowski.car.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Component
public class CreateUserDto {

    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private String email;
}
