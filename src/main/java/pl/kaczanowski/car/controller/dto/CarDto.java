package pl.kaczanowski.car.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.OffsetDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarDto {

    private Integer id;
    private String brand;
    private String model;
    private String name;
    private int year;
    private Integer millage;
    private OffsetDateTime insuranceDate;
    private OffsetDateTime motDate;
}
