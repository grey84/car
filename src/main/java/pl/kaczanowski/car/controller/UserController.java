package pl.kaczanowski.car.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kaczanowski.car.controller.dto.CreateUserDto;
import pl.kaczanowski.car.controller.dto.UpdateUserDto;
import pl.kaczanowski.car.controller.dto.UserDto;
import pl.kaczanowski.car.service.excepions.UserAlreadyExist;
import pl.kaczanowski.car.service.excepions.UserNotFoundExcepion;
import pl.kaczanowski.car.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable Integer id) throws UserNotFoundExcepion {
        return userService.getUserDtoById(id);
    }

    @PostMapping
    public UserDto createUser(@RequestBody CreateUserDto createUserDto) throws UserAlreadyExist {
        return userService.createUser(createUserDto);
    }

    @PutMapping("/{id}")
    public UserDto updateUser(@PathVariable Integer id, @RequestBody UpdateUserDto updateUserDto) throws UserNotFoundExcepion {
        return userService.updateUser(id, updateUserDto);
    }

    @DeleteMapping("/{id}")
    public UserDto deleteUser(@PathVariable Integer id) throws UserNotFoundExcepion {
        return userService.deleteUser(id);
    }
}
