package pl.kaczanowski.car.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kaczanowski.car.model.Car;
import pl.kaczanowski.car.repository.CarRepository;
import pl.kaczanowski.car.service.CarService;
import pl.kaczanowski.car.service.excepions.CarAlreadyExist;
import pl.kaczanowski.car.service.excepions.CarNotFoundExcepion;

import java.util.List;

@RestController
@RequestMapping("/api/v1/cars")
public class CarController {

    @Autowired
    CarService carService;

    @GetMapping
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable Integer id) throws CarNotFoundExcepion {
        return carService.getCarById(id);
    }

    @PostMapping
    public Car createCar(@RequestBody Car car) throws CarAlreadyExist {
        return carService.createCar(car);
    }

    @PutMapping("/{id}")
    public Car updateCar(@PathVariable Integer id, @RequestBody Car car) throws CarNotFoundExcepion {
        return carService.updateCar(id, car);
    }

    @DeleteMapping("/{id}")
    public Car deleteCar(@PathVariable Integer id) throws CarNotFoundExcepion {
        return carService.deleteCar(id);
    }
}
