package pl.kaczanowski.car.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String brand;
    private String model;
    private String name;
    private int year;
    private Integer millage;
    private OffsetDateTime insuranceDate;
    private OffsetDateTime motDate;

//    @ManyToOne
//    private User owner;
}
