package pl.kaczanowski.car.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kaczanowski.car.controller.dto.CarDto;
import pl.kaczanowski.car.controller.dto.UserDto;
import pl.kaczanowski.car.controller.mapper.CarDtoMapper;
import pl.kaczanowski.car.model.Car;
import pl.kaczanowski.car.model.User;
import pl.kaczanowski.car.repository.CarRepository;
import pl.kaczanowski.car.service.excepions.CarAlreadyExist;
import pl.kaczanowski.car.service.excepions.CarNotFoundExcepion;
import pl.kaczanowski.car.service.excepions.UserNotFoundExcepion;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {
    @Autowired
    CarRepository carRepository;
    @Autowired
    CarDtoMapper carDtoMapper;


    public Car getCarById(Integer id) throws CarNotFoundExcepion {
        return carRepository.findById(id)
                .orElseThrow(() -> new CarNotFoundExcepion());
    }

    public List<Car> getAllCars() {
        return carRepository.findAll().stream()
                .collect(Collectors.toList());
    }

    public Car createCar(Car car) throws CarAlreadyExist {
        carRepository.findById(car.getId())
                .orElseThrow(() -> new CarAlreadyExist());

        return carRepository.save(car);
    }

    public Car updateCar(Integer id, Car car) throws CarNotFoundExcepion {
        Car carById = getCarById(id);

        carById.setBrand(car.getBrand());
        carById.setModel(car.getModel());
        carById.setName(car.getName());
        carById.setMillage(car.getMillage());
        carById.setYear(car.getYear());
        carById.setInsuranceDate(car.getInsuranceDate());
        carById.setMotDate(car.getMotDate());

        return carRepository.save(carById);

    }

    public Car deleteCar(Integer id) throws CarNotFoundExcepion {
        Car carById = getCarById(id);
        carRepository.delete(carById);
        return carById;
    }
}
