package pl.kaczanowski.car.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kaczanowski.car.controller.dto.CreateUserDto;
import pl.kaczanowski.car.controller.dto.UpdateUserDto;
import pl.kaczanowski.car.controller.dto.UserDto;
import pl.kaczanowski.car.controller.mapper.UserDtoMapper;
import pl.kaczanowski.car.model.User;
import pl.kaczanowski.car.service.excepions.UserAlreadyExist;
import pl.kaczanowski.car.service.excepions.UserNotFoundExcepion;
import pl.kaczanowski.car.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserDtoMapper userDtoMapper;




    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream()
                .map(user -> userDtoMapper.mappingToDto(user))
                .collect(Collectors.toList());
    }

    public UserDto getUserDtoById(Integer id) throws UserNotFoundExcepion {
        return userRepository.findById(id)
                .map(user -> userDtoMapper.mappingToDto(user))
                .orElseThrow(() -> new UserNotFoundExcepion());
    }
    public User getUserById(Integer id) throws UserNotFoundExcepion {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundExcepion());
    }


    public UserDto createUser(CreateUserDto createUserDto) throws UserAlreadyExist {
        if (userRepository.findByLogin(createUserDto.getLogin())) {
            throw new UserAlreadyExist();
        }

        User user = userDtoMapper.mappingToModel(createUserDto);
        user.setPassword(createUserDto.getPassword());
        User createdUser = userRepository.save(user);

        return userDtoMapper.mappingToDto(createdUser);
    }

    public UserDto updateUser(Integer id, UpdateUserDto updateUserDto) throws UserNotFoundExcepion {
        User userById = getUserById(id);
        userById.setFirstName(updateUserDto.getFirstName());
        userById.setLastName(updateUserDto.getLastName());
        userById.setPassword(updateUserDto.getPassword());
        userById.setEmail(updateUserDto.getEmail());

        User updatedUser = userRepository.save(userById);
        return userDtoMapper.mappingToDto(updatedUser);

    }

    public UserDto deleteUser(Integer id) throws UserNotFoundExcepion {
        User userById = getUserById(id);
        userRepository.delete(userById);
        return userDtoMapper.mappingToDto(userById);
    }


}
